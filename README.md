# README #



### What is this repository for? ###

Use this template to create your own noteblog: a blog for Jupyter notebooks.

#### Quick summary

1. [Fork this repository](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)
2. Get a local clone of the fork on your system
3. Obtain and install a python distribution (e.g., [Anaconda python](https://www.continuum.io/downloads))
4. Install the Python pre-requisites using the `requirements.txt` file in the clone: `pip install -r requirements.txt`
5. Customize the `pelicanconf.py` file (particularly the `AUTHOR` and `SITENAME` fields)
5. (Optional): set the `THEME` field in `pelicanconf.py` (see http://www.pelicanthemes.com/ for examples)
6. Copy new Jupyter notebooks to the `incoming_notebooks` directory: e.g., `cp -p ./*.ipynb`
7. If you have never used git before: `git config --global user.email "you@example.com" && git config --global user.name "Your Name"`
8. Add these as noteblog entries: `./scripts/addIncoming.bash`
9. (Optional) start a simple webserver: `make serve` and view the NoteBlog in your browser: [http://localhost:8000](http://localhost:8000)

# Workflow
Once installed, the general workflow has two main steps:

1. Copy new Jupyter notebooks to the `incoming_notebooks` directory: e.g., `cp -p ./*.ipynb`
2. Add these as noteblog entries: `./scripts/addIncoming.bash`

For permanent use, it would be recommended to set up a web server that serves the site produced in the `output` directory of the noteblog installation.
