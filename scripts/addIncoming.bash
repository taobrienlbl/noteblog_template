#!/bin/bash

# Get the noteblog base directory
export SCRIPT_PATH=`python -c "import os; print(os.path.abspath(os.path.dirname(\"$0\")))"`
echo $SCRIPT_PATH

python ${SCRIPT_PATH}/addPost.py --clobber --remove --commit --rebuild ${SCRIPT_PATH}/../incoming_notebooks/*
